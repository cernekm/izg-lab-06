#include"shader.h"
#include<stdio.h>
#include<stdlib.h>
#include<SDL/SDL.h>
#include<iostream>
#include<fstream>
#include<sstream>


std::string loadShader(std::string fileName){
  std::ifstream f(fileName.c_str());
  if(!f.is_open()){
    std::cerr<<"file: "<<fileName<<" does not exist!"<<std::endl;
    return"";
  }
  std::string str((std::istreambuf_iterator<char>(f)),std::istreambuf_iterator<char>());
  const std::string includeKeyword="#include";
  for(;;){
    std::size_t pos = str.find(includeKeyword);
    if(pos==std::string::npos)break;
    std::size_t endpos = str.find("\n",pos);
    std::string fileWithSpaces = str.substr(pos+includeKeyword.size()+1,endpos-pos-includeKeyword.size()-1);
    std::string includedFile="";
    for(unsigned i=0;i<fileWithSpaces.length();++i)
      if(!std::isspace(fileWithSpaces[i]))
        includedFile+=fileWithSpaces[i];
    std::size_t dirpos=fileName.rfind("/");
    std::string dir="";
    if(dirpos!=std::string::npos)
      dir=fileName.substr(0,dirpos+1);
    std::string loaded=loadShader(dir+includedFile);
    str.replace(pos,endpos-pos,loaded);
    break;
  }
  f.close();
  return str;
}

char*loadFile(const char*FileName){
  loadShader(FileName);
	FILE*f=fopen(FileName,"rb");//open file
	if(!f){
		fprintf(stderr,"File %s does not exists",FileName);
		fflush(stderr);
		exit(1);
	}

	fseek(f,0,SEEK_END);//go to the end
	size_t FileSize=ftell(f);//get position
	fseek(f,0,SEEK_SET);//go to the start
	char*Result=(char*)malloc(sizeof(char)*(FileSize+1));//allocate memory
	fread(Result,sizeof(char),FileSize,f);//load file
	Result[FileSize]='\0';//add string end symbol
	return Result;//return loaded file
}

GLuint createShader(const char*Source,GLenum Type){
	GLuint Result=glCreateShader(Type);//create shader name
	GLchar*ptr[1]={(GLchar*)Source};//source
	glShaderSource(Result,1,(const GLchar**)ptr,NULL);//set shader source
	glCompileShader(Result);//compile shader
	int BufferLen;
	char Buffer[100000];
	glGetShaderiv(Result,GL_INFO_LOG_LENGTH,&BufferLen);
	glGetShaderInfoLog(Result,BufferLen,NULL,Buffer);
	if(BufferLen>0){
		fprintf(stderr,"%s\n",Buffer);
		fflush(stderr);
	}
	return Result;
}

GLuint createShaderProgramVSFS(const char*VS,const char*FS){
	GLuint VSId=createShader(loadShader(VS).c_str(),GL_VERTEX_SHADER);//vertex shader
	GLuint FSId=createShader(loadShader(FS).c_str(),GL_FRAGMENT_SHADER);//fragment shader
	GLuint Result=glCreateProgram();//create shader program
	glAttachShader(Result,VSId);//attach vertex shader to shader program
	glAttachShader(Result,FSId);//attach fragment shader to fragment program
	glLinkProgram(Result);//link shader
	return Result;//return shader program
}

GLuint createShaderProgramVSGSFS(const char*VS,const char*GS,const char*FS){
	GLuint VSId=createShader(loadShader(VS).c_str(),GL_VERTEX_SHADER);//vertex shader
  GLuint GSId=createShader(loadShader(GS).c_str(),GL_GEOMETRY_SHADER);//geometry shader
	GLuint FSId=createShader(loadShader(FS).c_str(),GL_FRAGMENT_SHADER);//fragment shader
	GLuint Result=glCreateProgram();//create shader program
	glAttachShader(Result,VSId);//attach vertex shader to shader program
  glAttachShader(Result,GSId);//attach geometry hsader to shader program
	glAttachShader(Result,FSId);//attach fragment shader to fragment program
	glLinkProgram(Result);//link shader
	return Result;//return shader program
}

GLuint loadTexture(const char*FileName){
	SDL_Surface*Surface=SDL_LoadBMP(FileName);
	if(Surface==NULL){
		fprintf(stderr,"File %s does not exists\n",FileName);
		return 0;
	}
	glPixelStorei(GL_UNPACK_ALIGNMENT,4);
	GLuint Result;
	glGenTextures(1,&Result);//generate name of the texture
	glBindTexture(GL_TEXTURE_2D,Result);//create texture as 2D texture
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);//set filtering
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);//set filtering
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);//set wrapping
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);//set wrapping

	if(	(Surface->format->Rmask==0xff0000)&&//GBR?
			(Surface->format->Gmask==0xff00)&&
			(Surface->format->Bmask==0xff)&&
			(Surface->format->Amask==0)){
		glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,Surface->w,Surface->h,
			0,GL_BGR,GL_UNSIGNED_BYTE,Surface->pixels);
	}else if(//RGB?
			(Surface->format->Rmask==0xff0000)&&
			(Surface->format->Gmask==0xff00)&&
			(Surface->format->Bmask==0xff)&&
			(Surface->format->Amask==0)){
		glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,Surface->w,Surface->h,
			0,GL_RGB,GL_UNSIGNED_BYTE,Surface->pixels);
	}

	return Result;
}

