#pragma once

#include<GL/glew.h>

GLuint createShaderProgramVSFS(const char*vs,const char*fs);
GLuint createShaderProgramVSGSFS(const char*vs,const char*gs,const char*fs);
GLuint loadTexture(const char*fileName);

