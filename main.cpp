/*
UKOLY V TOMTO SOUBORU:
//STUDENT DOPLNI 1.a) - nahrani dat "data" na GPU
//STUDENT DOPLNI 1.b) - ziskani cisla atributu do AttribPosition z shader programu
//STUDENT DOPLNI 1.c) - nastaveni ukazatelu na data a vykresleni valce

//STUDENT DOPLNI 4.a) - ziskani cisla uniformu z shader programu pro cas
//STUDENT DOPLNI 4.b) - nastaveni uniformni promenne cas

//STUDENT DOPLNI 5.a) - zapnuti blendingu pred vykreslenim a vypnuti zapisu do depth bufferu
*/

#include<stdio.h>
#include<GL/glew.h>
#include<GL/gl.h>
#include<SDL/SDL.h>
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_access.hpp>

#include"shader.h"
#include"stars.h"

void init();
void idle();
void setMatrix();

unsigned       windowSize[2]  = {1024,768};
const unsigned size           = 20;//number of cells in one row of fire
const unsigned nofStars       = 1000;//number of stars
float          cameraAngle[3] = {0.f,0.f,0.f};//angle of camera
int            keys[256]      = {0};
glm::vec3      cameraPosition = glm::vec3(0.f,0.f,-5.f);//camera position
float          time           = 0;//aktualni cas - rizene poctem snimku

glm::mat4 modelMatrix;//model matrix
glm::mat4 viewMatrix;//view matrix
glm::mat4 projectionMatrix;//projection matrix

//Program ID
GLuint program;

struct Uniforms{
  GLuint modelMatrix;
  GLuint viewMatrix;
  GLuint projectionMatrix;
  GLuint time;
}un;

//Attributes IDs
GLuint attribPosition;
//Array Buffer IDs
GLuint VBO;//vertex buffer object

void fire_Init(){
  unsigned N=10;//number of floats per vertex
  float*data = new float[size*size*2*3*N];

  unsigned XOffSet[6]={0,1,0,0,1,1};
  unsigned YOffSet[6]={0,0,1,1,0,1};
  for(unsigned y=0;y<size;++y){//loop over latitudes
    for(unsigned x=0;x<size;++x){//loo over longitudes
      unsigned base=y*size+x;
      for(unsigned k=0;k<6;++k){//loop over vetices of quad
        float radius=.3;
        float pi=3.14159265359;
        float angle=((float)(x+XOffSet[k])/(float)size*2-1)*pi*2;
        float ry=((float)(y+YOffSet[k])/(float)size*2-1);
        data[(base*6+k)*N+5]=cos(angle)*radius;
        data[(base*6+k)*N+6]=ry;
        data[(base*6+k)*N+7]=sin(angle)*radius;
      }
    }
  }

  //STUDENT DOPLNI 1.a) - nahrani dat "data" na GPU
  //glGenBuffers, glBindBuffer, glBufferData
  glGenBuffers(1,&VBO); // generovanie identifikatoru
  glBindBuffer(GL_ARRAY_BUFFER,VBO); // aktivovanie a vytvorenie VBO
  glBufferData(GL_ARRAY_BUFFER, sizeof(float)*size*size*2*3*N, data, GL_STATIC_DRAW); // alokacia bufferu a nahratie dat


  delete[]data;//free data

  program=createShaderProgramVSFS("shaders/fire.vp","shaders/fire.fp");//create shader program

  un.modelMatrix       = glGetUniformLocation(program,"m"       );
  un.viewMatrix        = glGetUniformLocation(program,"v"       );
  un.projectionMatrix  = glGetUniformLocation(program,"p"       );

  //STUDENT DOPLNI 1.b) - ziskani cisla atributu do AttribPosition z shader programu
  //napoveda: glGetAttribLocation
  attribPosition = glGetAttribLocation(program,"position");

  //STUDENT DOPLNI 4.a) - ziskani cisla uniformu z shader programu pro cas
  //napoveda: glGetUniformLocation
  un.time = glGetUniformLocation(program,"cas");
}

void fire_Draw(){
  glUseProgram(program);//use shader program

  //load matrices to shader program
  glUniformMatrix4fv(//load model matrix to shader program
      un.modelMatrix,//uniform ID in shader program
      1,//1 matrix
      GL_FALSE,//do not transpose matrix
      glm::value_ptr(modelMatrix));//data of the matrix

  glUniformMatrix4fv(//load view matrix to shader
      un.viewMatrix,//uniform ID in shader program
      1,//1 matrix
      GL_FALSE,//do not transpose matrix
      glm::value_ptr(viewMatrix));//data of the matrix

  glUniformMatrix4fv(//load projection matrix
      un.projectionMatrix,//uniform ID in shader program
      1,//1 matrix
      GL_FALSE,//do not transpose matrix
      glm::value_ptr(projectionMatrix));//data of the matrix

  //STUDENT DOPLNI 4.b) - nastaveni uniformni promenne cas
  //napoveda: glUniform1f
  glUniform1f(un.time,time);

  //STUDENT DOPLNI 1.c) - nastaveni ukazatelu na data a vykresleni valce
  //musite:
  //1. nabindovat buffer
  //2. povolit vertex atribut
  //3. nastavit jak se data v bufferu ulozena
  //4. vykreslit trojuhelniky
  //napoveda: glBindBuffer,glEnableVertexAttribArray, glVertexAttribPointer, glDrawArrays
  glBindBuffer(GL_ARRAY_BUFFER,VBO);
  glEnableVertexAttribArray(attribPosition);
  glVertexAttribPointer(
    attribPosition,   //ID atributu
    3,    //pocet slozek atributu 3D vektor
    GL_FLOAT,   //float type
    GL_FALSE,   //nenormalizujeme
    sizeof(float)*10,    //velikost vsech atributu v tomto VBO pro jeden vertex
    (GLvoid*)(sizeof(float)*5)    //offset v atributech
  );
  
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);
  glDepthMask(GL_FALSE);
  
  glDrawArrays(//vykresleni
    GL_TRIANGLES,//typ primitiva
    0,    //prvni Vertex
    2400    //pocet Vertexu pro vykresleni 3 -> 1 trojuhelnik
  );
  
  //STUDENT DOPLNI 5.a) - zapnuti blendingu pred vykreslenim a vypnuti zapisu do depth bufferu
  //napoveda glEnable, glBlendFunc, glDepthMask, glDisable
  
  glDepthMask(GL_TRUE);
  glDisable(GL_BLEND);
  

}

void idle(){
  setMatrix();//set model view projection matrix

  for(int i=0;i<3;++i)//compute new camera position
    cameraPosition+=(keys[(unsigned)"acw"[i]]-keys[(unsigned)"d s"[i]])*.01f*
      glm::vec3(glm::row(viewMatrix,i));

  glClearColor(0,0,0,1);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);//clear color & depth buffer

  //draw stars
  stars_Draw(
      glm::value_ptr(modelMatrix),
      glm::value_ptr(viewMatrix),
      glm::value_ptr(projectionMatrix));

  //draw fire
  fire_Draw();

  SDL_GL_SwapBuffers();//swap buffer 
  time+=.1;
}

void init(){
  glewInit();
  stars_Init(nofStars);
  fire_Init();

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
}

int main(int Argc,char*Argv[]){
  SDL_Init(SDL_INIT_VIDEO);//init video
  SDL_SetVideoMode(windowSize[0],windowSize[1],24,SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_OPENGL);
  init();//init OpenGL
  SDL_Event event;
  int running=1;
  float sensitivity=0.41;
  while(running){
    while(SDL_PollEvent(&event)){
      switch(event.type){
        case SDL_QUIT:
          running=0;
          break;
        case SDL_MOUSEMOTION:
          if(event.motion.state&SDL_BUTTON_LMASK){
            cameraAngle[1]+=event.motion.xrel*sensitivity;
            cameraAngle[0]+=event.motion.yrel*sensitivity;
          }
          break;
        case SDL_KEYDOWN:
		  if(event.key.keysym.sym==SDLK_q)running=0;
          keys[event.key.keysym.sym%255]=1;
          break;
        case SDL_KEYUP:
          keys[event.key.keysym.sym%255]=0;
          break;
      }
    }
    idle();
  }
  SDL_Quit();
  return 0;
}

void setMatrix(){
  const float fovy        = 45.f                              ;
  const float aspectRatio = (float)windowSize[0]/windowSize[1];
  const float near        = .1f                               ;
  const float far         = 10000.f                           ;

  projectionMatrix=glm::perspective(fovy,aspectRatio,near,far);//perspective matrix
  viewMatrix=//view matrix
    glm::rotate(glm::mat4(1.f),cameraAngle[2],glm::vec3(0,0,1))*
    glm::rotate(glm::mat4(1.f),cameraAngle[0],glm::vec3(1,0,0))*
    glm::rotate(glm::mat4(1.f),cameraAngle[1],glm::vec3(0,1,0))*
    glm::translate(glm::mat4(1.f),cameraPosition);
  modelMatrix=glm::mat4(1.f);//model matrix
}

