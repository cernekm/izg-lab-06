BINNAME=izg6

ifeq ($(OS),Windows_NT)
	LIB=-lmingw32 -lSDLmain -lSDL -lopengl32 -lGLEW32
	BIN=${BINNAME}.exe
	DEFS=WINDOWS
else
	LIB=-lSDL -lGLEW -lGL
	BIN=${BINNAME}
	DEFS=LINUX
endif

${BIN}: *.cpp *.h makefile
	echo $(OS)
	g++ *.cpp -o ${BIN} -Wall ${INC} ${LIB} -D${DEFS}

run: ${BIN}
	./${BIN}
	more stderr.txt

clean:
	rm -rf ${BIN}

	
