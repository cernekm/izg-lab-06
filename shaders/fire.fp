#version 330

/*
 include macro neni soucasti GLSL!
 ve cviceni je to reseno pri nacitani
 noise.vp obsahuje sumove funkce

 float noise(int   x,uint oct,uint usedOct);
 float noise(ivec2 x,uint oct,uint usedOct); 
 float noise(ivec3 x,uint oct,uint usedOct); 
 float noise(ivec4 x,uint oct,uint usedOct);
 ktere vraci pseudonahodny sum v rozsahu <-1,1>
 x       - pozice v 1D/2D/3D/4D dimenzi
 oct     - pocet oktav sumu
 usedOct - pocet vyuzitych oktav sumu

 priklad vyuziti float r = noise(ivec2(pozice.x,cas*10),5u,5u)*.5+.5;
 r bude obsahovat pseudonahodnou hodnotu v rozsahu <0,1>
*/
#include noise.vp     
BEGINGRADIENT(fireColors)
  vec4(0,0,0,0),
  vec4(1,0,0,1),
  vec4(1,1,0,1),
  vec4(1,1,1,1)
ENDGRADIENT

out vec4 fColor;//output color

//STUDENT DOPLNI 3.c) - vytvorte vstupni promennou pro pozici - navazte na promennou z VS
in vec3 wPosition;
//STUDENT DOPLNI 4.e) - vytvorte uniformni promennou pro cas
uniform float cas;

void main(){
  //fColor=vec4(1,0,0,0);//every fragment will be red

  //STUDENT DOPLNI 3.d) - obarvete valec pomoci sumu a barevneho prechodu
  //  - vyuzijte 3D funkci noise - vraci sum, ktery budete adresovat interpolovanou pozici vrcholu
  //  - sumem hybejte v zapornem smeru osy y pro rozpohybovani
  //  - pozice je v rozsahu <-1,1>; sum ma ale v tomto intervalu malo frekvenci
  //  - zvetsete interval pozice alespon 300x
  //  - integrovane intel GPU v CVT jsou pomale nastavte pocet oktav sumu na oct=5u a pocet
  //  - vyuzitych oktav na usedOct=3u
  //  - barvu ohne vyberte funkci fireColors(float t), ktera vraci pro t=<-1,1> barvu - vec4
  //fColor=fireColors(noise(ivec3(wPosition * 1000),5u,3u));
  

  //STUDENT DOPLNI 4.f) - rozhybejte barvy na valci pomoci casu
  // - vyuzijte  4D funkci noise
  fColor=fireColors(noise(ivec4(wPosition * 1000 + vec3(0,-10 * cas ,0), cas*10),5u,3u)) * (1 - wPosition.y);
  
}
