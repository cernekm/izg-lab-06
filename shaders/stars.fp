#version 330

uniform sampler2D StarsTexture;

in vec2     gCoord;

out vec4 fColor;

void main(){
  fColor=texture(StarsTexture,gCoord);
}
