#version 330

layout(points)in;
layout(triangle_strip,max_vertices=4)out;

uniform int NumTypes=1;
uniform mat4 m,v,p;

flat in int vId[];
in float vSize[];

out vec2 gCoord;

void main(){
  vec4 VP=v*m*gl_in[0].gl_Position;
  for(int i=0;i<4;++i){
    gl_Position=p*(VP+vSize[0]*vec4(-1.+2.*(i%2),-1.+2.*(i/2),0,0));
    //gl_Position=vec4(.1*(i%2),.1*i/2,0,1);
    gCoord=vec2(i%2,i/2)/vec2(NumTypes,1)+vec2((vId[0]%NumTypes)/float(NumTypes),0);
    EmitVertex();
  }
  EndPrimitive();
}
