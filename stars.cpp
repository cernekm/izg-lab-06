#include"stars.h"
#include"shader.h"
#include<GL/glew.h>

struct SStarsProgram{
  GLuint Program;
  GLuint UniformModelMatrix;
  GLuint UniformViewMatrix;
  GLuint UniformProjectionMatrix;
  GLuint UniformStarsTexture;
  GLuint UniformNumTypes;
  GLuint StarsTexture;
  unsigned NumStars;
}SP;

void stars_Init(unsigned NumStars){
  SP.Program=createShaderProgramVSGSFS("shaders/stars.vp","shaders/stars.gp","shaders/stars.fp");
  SP.UniformModelMatrix      = glGetUniformLocation(SP.Program,"m");
  SP.UniformViewMatrix       = glGetUniformLocation(SP.Program,"v");
  SP.UniformProjectionMatrix = glGetUniformLocation(SP.Program,"p");
  SP.UniformStarsTexture     = glGetUniformLocation(SP.Program,"StarsTexture");
  SP.UniformNumTypes         = glGetUniformLocation(SP.Program,"NumTypes");
  SP.StarsTexture            = loadTexture("stars.bmp");
  SP.NumStars                = NumStars;
}

void stars_Draw(float*m,float*v,float*p){
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE,GL_ONE);
  glUseProgram(SP.Program);

	glUniformMatrix4fv(SP.UniformModelMatrix,1,GL_FALSE,m);
	glUniformMatrix4fv(SP.UniformViewMatrix,1,GL_FALSE,v);
	glUniformMatrix4fv(SP.UniformProjectionMatrix,1,GL_FALSE,p);

  glUniform1i(SP.UniformNumTypes,6);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D,SP.StarsTexture);
  glUniform1ui(SP.UniformStarsTexture,0);

  glBindBuffer(GL_ARRAY_BUFFER,0);
  glDrawArrays(GL_POINTS,0,SP.NumStars);

  glDisable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
}
